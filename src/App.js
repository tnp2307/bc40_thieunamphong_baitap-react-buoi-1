import logo from './logo.svg';
import './App.css';
import ExBaiTap from './ExBaiTap/ExBaiTap';

function App() {
  return (
    <div className="App">
      <div className='container bg-light my-5'>
        <h1>A warm welcome</h1>
        <h5 className=''>Bootstrap utility classes are used to create this jumbotron since the old component has been removed from the framework. Why create custom CSS when you can use utilities?</h5>
        <button className='btn btn-primary'>Call to action</button>
      </div>
      <div className="row">
        <ExBaiTap/>
        <ExBaiTap/>
        <ExBaiTap/>
        <ExBaiTap/>
        <ExBaiTap/>
        <ExBaiTap/>
      </div>
      
    </div>
  );
}

export default App;
