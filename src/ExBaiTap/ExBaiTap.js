import React, { Component } from "react";

export default class ExBaiTap extends Component {
  render() {
    return (
      <div>
        <div className="container col-3 my-2" >
          <div className="card" style={{ width: "18rem" }}>
            <div className="card-body">
              <a href="#" className="btn btn-primary">
                Go somewhere
              </a>
              <h5 className="card-title">Card title</h5>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
